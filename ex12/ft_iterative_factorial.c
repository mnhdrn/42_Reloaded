/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_interative_factorial.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 00:02:28 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/06 18:03:53 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_calculate(int nb)
{
	int			tmp;
	int			i;

	tmp = (nb - 1);
	i = 0;
	while (i <= nb)
	{
		if (tmp > 0)
			nb *= tmp;
		i++;
		tmp--;
	}
	return (nb);
}

int				ft_iterative_factorial(int nb)
{
	if (nb == 0 || nb == 1)
		return (1);
	else if (nb > 1 && nb < 13)
		return (ft_calculate(nb));
	else
		return (0);
}
