/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/17 13:52:02 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/06 14:35:55 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "display.h"

void		ft_putchar(char c)
{
	write(1, &c, 1);
}

void		ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

int			main(int ac, char **av)
{
	int		fd;
	int		size;
	char	buffer[1024 + 1];

	if (ac < 2)
		ft_putstr("File name missing.\n\0");
	else if (ac > 2)
		ft_putstr("Too many arguments.\n\0");
	else
	{
		fd = open(av[1], O_RDONLY);
		while ((size = read(fd, buffer, 1024)) != 0)
		{
			buffer[size] = '\0';
			ft_putstr(buffer);
		}
		close(fd);
	}
	return (0);
}
